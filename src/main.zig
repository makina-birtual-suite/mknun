const std = @import("std");
const stdout_file = std.io.getStdOut().writer();

const inst_type_mask = 7 << 61;
const inst_func_mask = 15 << 57;
const dest_regs_mask = 7 << 54;
const src1_regs_mask = 7 << 51;
const src2_regs_mask = 7 << 48;

const Instruction = struct {
    inst_name: []const u8,
    take_dest: bool,
    take_src1: bool,
    take_src2: bool,
    take_immd: bool,
    const Self = @This();
    pub fn new(inst_name: []const u8, take_dest: bool, take_src1: bool, take_src2: bool, take_immd: bool) Self {
        return .{ .inst_name = inst_name, .take_dest = take_dest, .take_src1 = take_src1, .take_src2 = take_src2, .take_immd = take_immd };
    }
};

fn getInst(inst_type: u8, inst_func: u8) Instruction {
    switch (inst_type) {
        0 => switch (inst_func) {
            0 => return Instruction.new("li", true, false, false, true),
            14 => return Instruction.new("lui", true, false, false, true),
            1 => return Instruction.new("lb", true, true, false, false),
            2 => return Instruction.new("lq", true, true, false, false),
            3 => return Instruction.new("lh", true, true, false, false),
            4 => return Instruction.new("lw", true, true, false, false),
            5 => return Instruction.new("sb", true, true, false, false),
            6 => return Instruction.new("sq", true, true, false, false),
            7 => return Instruction.new("sh", true, true, false, false),
            8 => return Instruction.new("sw", true, true, false, false),
            9 => return Instruction.new("pop", true, false, false, false),
            10 => return Instruction.new("push", false, true, false, false),
            11 => return Instruction.new("ls", true, false, false, true),
            12 => return Instruction.new("ss", false, true, false, true),
            13 => return Instruction.new("ncall", false, false, false, true),
            else => std.process.exit(0),
        },
        1 => switch (inst_func) {
            0 => return Instruction.new("add", true, true, true, false),
            1 => return Instruction.new("addi", true, true, false, true),
            2 => return Instruction.new("sub", true, true, true, false),
            3 => return Instruction.new("subi", true, true, false, true),
            4 => return Instruction.new("mul", true, true, true, false),
            5 => return Instruction.new("muli", true, true, false, true),
            6 => return Instruction.new("div", true, true, true, false),
            7 => return Instruction.new("divi", true, true, false, true),
            8 => return Instruction.new("mod", true, true, true, false),
            9 => return Instruction.new("modi", true, true, false, true),
            else => std.process.exit(0),
        },
        2 => switch (inst_func) {
            0 => return Instruction.new("addf", true, true, true, false),
            1 => return Instruction.new("subf", true, true, true, false),
            2 => return Instruction.new("mulf", true, true, true, false),
            3 => return Instruction.new("divf", true, true, true, false),
            4 => return Instruction.new("modf", true, true, true, false),
            else => std.process.exit(0),
        },
        3 => switch (inst_func) {
            0 => return Instruction.new("itof_32", true, true, false, false),
            1 => return Instruction.new("itof_64", true, true, false, false),
            2 => return Instruction.new("ftoi_32", true, true, false, false),
            3 => return Instruction.new("ftoi_64", true, true, false, false),
            else => std.process.exit(0),
        },
        4 => switch (inst_func) {
            0 => return Instruction.new("eq_u", false, true, true, false),
            1 => return Instruction.new("eq_i", false, true, true, false),
            2 => return Instruction.new("cmp_u", false, true, true, false),
            3 => return Instruction.new("cmp_i", false, true, true, false),
            4 => return Instruction.new("ez_u", false, true, false, false),
            5 => return Instruction.new("ez_i", false, true, false, false),
            6 => return Instruction.new("size", true, true, false, false),
            else => std.process.exit(0),
        },
        5 => switch (inst_func) {
            0 => return Instruction.new("eq_f", false, true, true, false),
            1 => return Instruction.new("cmp_f", false, true, true, false),
            2 => return Instruction.new("ez_f", false, true, false, false),
            3 => return Instruction.new("raw", true, true, false, false),
            else => std.process.exit(0),
        },
        6 => switch (inst_func) {
            0 => return Instruction.new("jmp", false, false, false, true), // Jumps are assumed to be immd jumps
            1 => return Instruction.new("jez", false, false, false, true), // Jumps are assumed to be immd jumps
            2 => return Instruction.new("jnz", false, false, false, true), // Jumps are assumed to be immd jumps
            3 => return Instruction.new("jge", false, false, false, true), // Jumps are assumed to be immd jumps
            4 => return Instruction.new("jlt", false, false, false, true), // Jumps are assumed to be immd jumps
            5 => return Instruction.new("call", false, false, false, true), // Jumps are assumed to be immd jumps
            6 => return Instruction.new("hlt", false, false, false, false), // Jumps are assumed to be immd jumps
            7 => return Instruction.new("ret", false, false, false, false), // Jumps are assumed to be immd jumps
            else => std.process.exit(0),
        },
        7 => switch (inst_func) {
            0 => return Instruction.new("and", true, true, true, false),
            1 => return Instruction.new("or", true, true, true, false),
            2 => return Instruction.new("xor", true, true, true, false),
            3 => return Instruction.new("not", true, true, false, false),
            4 => return Instruction.new("shl", true, true, true, false),
            5 => return Instruction.new("shr", true, true, true, false),
            else => std.process.exit(0),
        },
        //TODO!: Rest
        else => return Instruction.new("unknown?", false, false, false, false),
    }
}

fn getRegFromNum(reg: u8) u8 {
    return reg + 'a';
}

pub fn main() !void {
    // ----------------ALLOCATOR---------------------- //
    // Declare Program's Allocator
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();
    // ----------------ALLOCATOR---------------------- //
    // ----------------STDOUT------------------------- //
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();
    // ----------------STDOUT------------------------- //
    // ----------------ARGS--------------------------- //
    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);
    var debug = false;
    var filename = args[1];
    //std.debug.print("Args : {s}\n", .{args});
    if (args.len == 1) {
        std.debug.print("USAGE: mknun <file>", .{});
    } else if (args.len >= 2) {
        if (args.len == 3) {
            if (std.mem.eql(u8, args[1], "-d")) {
                debug = true;
                filename = args[2];
            } else if (std.mem.eql(u8, args[2], "-d")) {
                debug = true;
                filename = args[1];
            }
        }
        var file = try std.fs.cwd().openFile(filename, .{});
        defer file.close();
        const size_limit = std.math.maxInt(u64);
        var file_contents: []u8 = try file.readToEndAlloc(allocator, size_limit);
        defer allocator.free(file_contents);
        file_contents = file_contents[0 .. file_contents.len - (file_contents.len % 8)];
        const program = std.mem.bytesAsSlice(u64, file_contents);
        var i: u64 = 0;
        for (program) |curr_inst| {
            const curr_inst1 = std.mem.littleToNative(u64, curr_inst);
            const inst_type = @intCast(u8, (curr_inst1 & inst_type_mask) >> 61);
            const inst_func = @intCast(u8, (curr_inst1 & inst_func_mask) >> 57);
            var inst_name = switch (inst_type == 0 or inst_type == 1 or inst_type == 4) {
                true => getInst(inst_type, inst_func),
                false => getInst(inst_type, inst_func >> 1),
            };
            //std.debug.print("{} : ", .{inst_name});
            if (debug) {
                try stdout.print("{d}-{d}: ", .{ i, i + 7 });
            }
            i += 8;
            try stdout.print("{s} ", .{inst_name.inst_name});
            if (inst_name.take_dest) {
                const dest_regs = @intCast(u8, (curr_inst1 & dest_regs_mask) >> 54);
                try stdout.print("{c} ", .{getRegFromNum(dest_regs)});
            }
            if (inst_name.take_src1) {
                const src1_regs = @intCast(u8, (curr_inst1 & src1_regs_mask) >> 51);
                try stdout.print("{c} ", .{getRegFromNum(src1_regs)});
            }
            if (inst_name.take_src2) {
                const src2_regs = @intCast(u8, (curr_inst1 & src2_regs_mask) >> 48);
                try stdout.print("{c} ", .{getRegFromNum(src2_regs)});
            }
            if (inst_name.take_immd) {
                const immd = @bitCast(i32, @truncate(u32, curr_inst1));
                try stdout.print("{d}", .{immd});
            }
            try stdout.print("\n", .{});
            try bw.flush();
        }
    }
}
